#include "stdio.h"

extern int random ();

int func1 (int a, int b)
{
  return a+b;
}

int main (void)
{
  int a, b;

  a = random ();
  if (a)
    a = a + func1 (a, b);

  printf ("result=%d\n", a);

  return 0;
}
