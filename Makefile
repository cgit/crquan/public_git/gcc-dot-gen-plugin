
GCC = gcc
CC  = gcc

PLUGIN_FILE = dotgen.so
PLUGIN_SOURCE_FILES = pass-dgen.c
PLUGIN_OBJECT_FILES = $(patsubst %.c,%.o,$(PLUGIN_SOURCE_FILES))

GCCPLUGINS_DIR := $(shell $(GCC) -print-file-name=plugin)
CFLAGS += -I$(GCCPLUGINS_DIR)/include -I$(with_gmp)/include -Wall -fPIC -O2

all: $(PLUGIN_FILE)

$(PLUGIN_FILE): $(PLUGIN_OBJECT_FILES)
	$(GCC) -shared -o $@ $^

.PHONY: clean all test

test:
	@if [ ! -d dump-files ]; then mkdir dump-files; fi
	$(GCC) -fplugin=./$(PLUGIN_FILE) \
		-fdump-tree-all -dumpdir dump-files/ \
		-c test1.c

clean:
	@-rm -f *~ *.o $(PLUGIN_FILE)
